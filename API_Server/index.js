const express = require('express');
const cors = require('cors');
const app = express();
app.use(cors());
app.use (express.json());

const fs = require('fs');

let rawdata = fs.readFileSync('./mockdata/Payload.json');
let data = JSON.parse(rawdata);
rawdata = fs.readFileSync('./mockdata/UIConfig.json');
let uiconfig =  JSON.parse(rawdata);


app.get('/', (req, res) => {
    res.send('<h1>Image Slider</h1>')
});

app.get('/api/thumbs', (req, res) => {
    res.json(data);
});

app.get('/api/thumbs/:id', (req, res) => {
    const id = req.params.id;
    const img = data.find(thumb => thumb.id === id);
    if (img) {
        res.json(img);
    }else{
        res.status(404).json('').end();
    }
});

app.get('/api/uiconfig', (req, res) => {
    res.json(uiconfig);
});
app.post('/api/uiconfig/save', (req, res) => {
    let obj = req.body;
    let data = JSON.stringify(obj);
    fs.writeFileSync('./mockdata/UIConfig.json', data);
});
app.get('*', (req, res) =>{
    res.status(404).json('').end();
  });

const PORT = 3001;
app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});