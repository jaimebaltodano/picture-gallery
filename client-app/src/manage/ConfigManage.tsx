import React, { ChangeEvent } from 'react';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import { Segment, Form, Header, Button, Input, Select } from 'semantic-ui-react';
import { useStore } from '../app/stores/store';
import { SemanticWIDTHS } from 'semantic-ui-react/dist/commonjs/generic';

interface Config {
  imagePerLoad: number,
  thumbnailsCarouselWidth: SemanticWIDTHS,
  thumbnailsFolder: string,
  largeImagesFolder: string,
  dashBoardImageGalleryWidth: SemanticWIDTHS,
  dashBoardImageDetailWidth: SemanticWIDTHS,
  orderBy: string,
}

const ConfigManage = () => {
  const { settingStore } = useStore();
  const { saveSettings, config } = settingStore;

  const [uiParams, setUIParams] = useState<Config>({
    imagePerLoad: config?.imagePerLoad!,
    largeImagesFolder: config?.largeImagesFolder!,
    thumbnailsFolder: config?.thumbnailsFolder!,
    orderBy: config?.orderBy!,
    thumbnailsCarouselWidth: config?.thumbnailsCarouselWidth!,
    dashBoardImageGalleryWidth: config?.dashBoardImageGalleryWidth!,
    dashBoardImageDetailWidth: config?.dashBoardImageDetailWidth!,
  })

  const handleInputChange = (event: ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
    const { name, value } = event.target;
    setUIParams({ ...uiParams, [name]: value })
  }

  const handleSubmit = () => {
    saveSettings(uiParams)
  }

  return (
    <Segment clearing>
      <Header as="h2">
        Config Piga UI params
      </Header>
      <Form onSubmit={handleSubmit} autoComplete='off'>
        <Form.Field>
          <label>Image per Page</label>
          <Input placeholder='Image per Page' value={uiParams.imagePerLoad} name="imagePerLoad" onChange={handleInputChange} />
        </Form.Field>
        <Form.Field>
          <label>Images Folder</label>
          <Input placeholder='Image folder' value={uiParams.largeImagesFolder} name="largeImagesFolder" onChange={handleInputChange} />
        </Form.Field>
        <Form.Field>
          <label>Thumbnails Folder</label>
          <Input placeholder='Thumbnails Folder' value={uiParams.thumbnailsFolder} name="thumbnailsFolder" onChange={handleInputChange} />
        </Form.Field>
        <Form.Field>
          <label>Order By</label>
          <Input placeholder='Order By' value={uiParams.orderBy} name="orderBy" onChange={handleInputChange} />
        </Form.Field>
        <Form.Field>
          <label>Image Gallery Width</label>
          <Input placeholder='Image Gallery Width' value={uiParams.dashBoardImageGalleryWidth} name="dashBoardImageGalleryWidth" onChange={handleInputChange} />
        </Form.Field>
        <Form.Field>
          <label>Image Detail Width</label>
          <Input placeholder='Image Detail Width' value={uiParams.dashBoardImageDetailWidth} name="dashBoardImageDetailWidth" onChange={handleInputChange} />
        </Form.Field>
        <Button floated='left' positive type='submit' content='Submit' />
        <Button as={Link} to="/gallery" floated="left" type="button" content="Cancel" />
      </Form>
    </Segment>
  )
}

export default ConfigManage;