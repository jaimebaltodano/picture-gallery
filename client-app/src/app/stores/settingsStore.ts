import { makeAutoObservable } from "mobx";
import agent from "../api/agent";
import { UIConfig } from "../models/uiconfig";


export default class SettingsStore {
	config: UIConfig | undefined = undefined;
  loadingInitial = false;

	constructor() {
		makeAutoObservable(this)
	}

  setLoadingInitial = (state: boolean) => {
		this.loadingInitial = state;
	}

	loadSettings = async () => {
		this.setLoadingInitial(true);
		try {
			const configapi = await agent.Config.list();
      this.config = configapi;
			this.setLoadingInitial(false);
		}
		catch (error) {
			console.log(error)
			this.setLoadingInitial(false);
		}
	}

	saveSettings = async(config: UIConfig) => {
		try{
			await agent.Config.save(config)
		}
		catch(error){
			console.log(error);
		}
	}

}