import { createContext, useContext } from "react";
import ThumbNailStore from "./thumbNailStore";
import SettingsStore from "./settingsStore";

interface Store {
    thumbNailStore: ThumbNailStore,
    settingStore: SettingsStore
}

export const store: Store={
    thumbNailStore: new ThumbNailStore(),
    settingStore: new SettingsStore()
}

export const StoreContext = createContext(store);

export function useStore(){
    return useContext(StoreContext)
}