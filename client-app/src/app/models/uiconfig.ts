
import { SemanticWIDTHS } from 'semantic-ui-react/dist/commonjs/generic';

export interface UIConfig{
  imagePerLoad: number,
  thumbnailsCarouselWidth: SemanticWIDTHS,
  thumbnailsFolder: string,
  largeImagesFolder: string,
  dashBoardImageGalleryWidth: SemanticWIDTHS,
  dashBoardImageDetailWidth: SemanticWIDTHS,
  orderBy: string,
}