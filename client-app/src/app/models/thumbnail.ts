export interface ThumbNail {
    [title:string]: string;
    cost: string;
    id: string;
    description: string;
    thumbnail: string;
    image: string;
}