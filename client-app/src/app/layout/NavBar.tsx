import React from 'react';
import { Container, Menu } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';

const NavBar = () => {

	return (
		<Menu inverted fixed='top'>
			<Container>
				<Menu.Item header>
					<img src='assets/logo.png' alt='logo' style={{ marginRight: '10px' }} />
				</Menu.Item>
				<Menu.Item as={NavLink} to="/gallery" name='Gallery' />
				<Menu.Item as={NavLink} to="/config" name='Config' />
			</Container>
		</Menu>
	)
}

export default NavBar;