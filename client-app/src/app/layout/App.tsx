import React, { Fragment, useEffect } from 'react';
import { Container } from 'semantic-ui-react';
import NavBar from './NavBar';
import LoadingComponent from './LoadingComponent';
import DashBoard from '../../features/thumbnails/DashBoard';
import ConfigManage from '../../manage/ConfigManage';
import { observer } from 'mobx-react-lite';
import { useStore } from '../stores/store';
import { Route, Switch } from 'react-router-dom';

function App() {
  const { thumbNailStore, settingStore } = useStore();
  const { config } = settingStore;

  useEffect(() => {
    thumbNailStore.loadThumbNails();
    if (config) {
      thumbNailStore.sortThumbnailsBy(config.orderBy);
    }
  }, [thumbNailStore, config]);

  useEffect(() => {
    settingStore.loadSettings();
  }, [settingStore]);

  if (settingStore.loadingInitial) return <LoadingComponent content='Loading Settings' />
  if (thumbNailStore.loadingInitial) return <LoadingComponent content='Loading Images' />

  return (
    <Fragment>
      <NavBar />
      <Container style={{ marginTop: '4em' }}>
        <Switch>
          <Route exact path="/gallery" component={DashBoard} />
          <Route exact path="/config" component={ConfigManage} /> 
        </Switch>
      </Container>
    </Fragment>
  );
}

export default observer(App);
