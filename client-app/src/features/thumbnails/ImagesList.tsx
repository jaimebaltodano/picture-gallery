import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react-lite';
import { useStore } from '../../app/stores/store';
import { ThumbNail } from '../../app/models/thumbnail';
import { Button, Grid, Label, Segment } from 'semantic-ui-react';
import LoadingComponent from '../../app/layout/LoadingComponent';
import Item from './Item';

const ImagesList = () => {
	const { thumbNailStore, settingStore } = useStore();
	const { thumbNails } = thumbNailStore;
	const { config } = settingStore;
	const [pageNumber, setPageNumber] = useState<number>(0);
	const [thumbNailsGroup, setThumbNailsGroup] = useState<ThumbNail[]>([]);
	const [nextPage, setNextPage] = useState(true);
	const [itemsPerLoad, setItemsPerLoad] = useState(0);
	const [loadingThumbnails, setLoadingThumbnails] = useState(false);

	useEffect(() => {
		const ipl = config?.imagePerLoad;
		if (ipl !== undefined) {
			const items = ipl!;
			setItemsPerLoad(items);
			const lastItem = pageNumber * items + items;
			setNextPage(lastItem < thumbNails.length);
			setThumbNailsGroup(thumbNails.slice(pageNumber * items, lastItem));
		}
	}, [thumbNails, pageNumber, config]);

	const computeNewPage = (orientation: string) => {
		setLoadingThumbnails(true);
		const lastItem = pageNumber * itemsPerLoad + itemsPerLoad;
		if (orientation === 'left') {
			if (pageNumber > 0)
				setPageNumber(pageNumber - 1);
		} else {
			if (lastItem < thumbNails.length)
				setPageNumber(pageNumber + 1);
		}
		setLoadingThumbnails(false);
	}

	if (loadingThumbnails) return (<Segment><LoadingComponent /></Segment>)


	return (
		<Segment>
			<Grid container doubling centered columns={config?.thumbnailsCarouselWidth}>
				<Grid.Row verticalAlign='top'>
					<Grid.Column textAlign='center'>
						<Button className={(pageNumber > 0) ? "positive" : "basic disabled"} circular icon='arrow alternate circle left' size='big' color="grey" onClick={() => computeNewPage('left')} />
					</Grid.Column>
					<Grid.Column textAlign='center'>
						<Button className={(nextPage) ? "positive" : "basic disabled"} icon='arrow alternate circle right' circular size='big' color="grey" onClick={() => computeNewPage('right')} />
					</Grid.Column>
				</Grid.Row>
				<Grid.Row verticalAlign='middle' style={{ paddingTop: '0px' }}>
					{thumbNailsGroup.map(thumbnail => (
						<Grid.Column key={thumbnail.id}>
							<Segment textAlign='center'>
								<Label attached='top'>{thumbnail.id}</Label>
								<Item src={`${config?.thumbnailsFolder}${thumbnail.thumbnail}`} id={thumbnail.id} />
							</Segment>
						</Grid.Column>
					))}
				</Grid.Row>
			</Grid>
		</Segment>
	)
}

export default observer(ImagesList);