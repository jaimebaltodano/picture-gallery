import React from 'react';
import { Grid } from 'semantic-ui-react';
import ImagesList from './ImagesList';
import ImageDetails from '../details/ImageDetails';
import { useStore } from '../../app/stores/store';
import { observer } from 'mobx-react-lite';

const DashBoard = () => {

	const { thumbNailStore, settingStore } = useStore();
	const { selectedImage } = thumbNailStore;
	const { config } = settingStore;
	return (
		<Grid padded centered>
			<Grid.Row>
				<Grid.Column width={config?.dashBoardImageDetailWidth}>
					{selectedImage &&
						<ImageDetails />}
				</Grid.Column>
			</Grid.Row>
			<Grid.Row>
				<Grid.Column width={config?.dashBoardImageGalleryWidth}>
					<ImagesList />
				</Grid.Column>
			</Grid.Row>
		</Grid>
	)
}

export default observer(DashBoard)