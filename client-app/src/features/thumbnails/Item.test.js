import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render } from '@testing-library/react';
import Item from "./Item";

describe("Test Item", () => {
  let component = undefined;
  beforeEach(() => {
    const item = {
      id: "003",
      scr: 'test.jpeg'
    }
    component = render(<Item id={item.id} src={item.scr} />);
  })

  test('render Item', () => {

    console.log(component);
    //expect (component.container).toHaveTextContent(item.id);
  })


  test('click on thumbnail', () => {
    const button = component.getByText("img");
    fireEvent.click(button);

  })
})