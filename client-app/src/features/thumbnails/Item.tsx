import React, { useEffect, useState } from 'react';
import { Image } from 'semantic-ui-react';
import { useStore } from '../../app/stores/store';
import { observer } from 'mobx-react-lite';
import './styles.css';
import LoadingComponent from '../../app/layout/LoadingComponent';

interface Props {
  id: string,
  src: string
} 
const Item = ({id, src}: Props) => {
  const {thumbNailStore} = useStore();
  const {selectedImage} = thumbNailStore;
  const [loading, setLoading] = useState(true);
  const [imageSRC, setImageSRC] = useState('');
  const [imageHolder, setImageHolder] = useState(React.createElement("img"));

  useEffect(() => {
    let i = React.createElement("img", {
      src: src,
      onLoad: () => {
        setLoading(false);
        setImageSRC(src);
      }
    });
    setImageHolder(i);
  }, [src])

  if (loading) return (<><LoadingComponent />{imageHolder}</>)

   return(
     <Image 
     size="massive"
     className={selectedImage?.id === id ? 'imageborder' : ''}
     src={imageSRC}
     onClick={() => thumbNailStore.selectImage(id)}
     />
   )
}

export default observer(Item);