import React from 'react';
import { Card, Image, Header, Segment, Grid, Divider } from 'semantic-ui-react';
import LoadingComponent from '../../app/layout/LoadingComponent';
import { useStore } from '../../app/stores/store';
import './styles.css';


const ImageDetails = () => {
	const { thumbNailStore, settingStore } = useStore();
	const { selectedImage: image } = thumbNailStore;
	const { config } = settingStore;

	if (!image) return <LoadingComponent />;

	return (
		<Card fluid>
			<Segment>
				<Header size="huge" floated="left">
					{image.title}
				</Header>
				<Divider clearing />
				<Grid columns='2'>
					<Grid.Column>
						<Segment>
							<Card.Content>
								<Card.Description as="big">
									{image.description}
								</Card.Description>
							</Card.Content>
						</Segment>
					</Grid.Column>
					<Grid.Column >
						<Segment textAlign='center'>
						<Image
							size="big"
							label={{
								as: 'a',
								size:'big',
								color: 'teal',
								content: ' ' + image.cost,
								icon: 'cart',
								ribbon: true,
							}}
							src={`${config?.largeImagesFolder}${image.image}`} />
							</Segment>
					</Grid.Column>
				</Grid>
			</Segment>
		</Card>


	);
}

export default ImageDetails;